extends Node

var channel
	
func _input(event):
	# POC: send keypresses over the channel
	if channel != null:
		if event is InputEventKey:
			channel.send(event.as_text())

func _process(delta):
	# POC: if receiving messages on the channel, just print them out
	if channel != null:
		for message in channel.recv():
			print(message)
	elif %Lobby.has_connection():
		# NOTE: this is important -- don't attempt to grab a pointer to the channel until the Lobby
		# has a valid connection ready
		channel = %Lobby.channel() # NOTE: Lobby frees itself after returning the channel
