extends Node

# These signals can be connected to by a UI lobby scene or the game scene.
signal player_connected(peer_id, player_info)
signal player_disconnected(peer_id)
signal server_disconnected

var PORT
var DEFAULT_SERVER_IP
var MAX_CONNECTIONS

var players: Dictionary = {}
var players_loaded = 0

func new(PORT, DEFAULT_SERVER_IP, MAX_CONNECTIONS):
	self.PORT = PORT
	self.DEFAULT_SERVER_IP = DEFAULT_SERVER_IP
	self.MAX_CONNECTIONS = MAX_CONNECTIONS
	return self

func _ready():
	print("I am a server")
	multiplayer.peer_disconnected.connect(_on_player_disconnected)
	create_game()

func create_game():
	var peer = ENetMultiplayerPeer.new()
	var error = peer.create_server(PORT, MAX_CONNECTIONS)
	if error:
		return error
	multiplayer.multiplayer_peer = peer
	print("Server listening on: ", DEFAULT_SERVER_IP, ":", PORT)
	
	
func erase_player(id):
	players.erase(id)


@rpc("any_peer", "reliable")
func load_game(game_scene_path):
	pass

@rpc("any_peer", "reliable")
func register_player(new_player_info):
	var new_player_id = multiplayer.get_remote_sender_id()
	players[new_player_id] = new_player_info
	player_connected.emit(new_player_id, new_player_info)
	if len(players) > 1:
		print("start RTC handshake")
		

func _on_player_disconnected(id):
	print("player disconnected: ", id)
	players.erase(id)
	player_disconnected.emit(id)

@rpc("any_peer", "reliable")
func send_description(type, sdp, recipient, sender):
	#var sender = players.get(multiplayer.get_remote_sender_id(), {}).get("name", null)
	#var recipient = players.get(id, {}).get("name", null)
	print("Relaying ", type, " description from ", sender, " to ", recipient)
	#print(id)
	receive_description.rpc_id(recipient, type, sdp, sender)
	
@rpc("any_peer", "reliable")
func receive_description(type, sdp, sender):
	pass

@rpc("any_peer", "reliable")
func send_candidate(media, index, name, recipient, sender):
	send_candidate.rpc_id(recipient, media, index, name, sender, recipient)
