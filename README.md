# Drop in WebRTC Lobby for Godot

Goal: despite some tutorials for WebRTC in Godot, there was no "drop in" way to say: "here's some servers, give me a connection". This does that -- as the developer you can remain blissfully ignorant of the complicated details of WebRTC and just get yourself a direct data channel between two peers.

## Requirements

If you're going to run this anywhere other than HTML5, you must have the native WebRTC addon: https://github.com/godotengine/webrtc-native

## Proof-of-Concept

 1. Run the server (which doesn't really do anything other than instantiate the lobby) via something like `godot --headless game_server.tscn`
 2. Setup your editor to trigger two instances of the main `game.tscn` scene.
 3. Play the scene, experience the glory of key-presses in one being printed by the other.

## Production

Copy the `RTC` folder into your Godot project. If there's enough interest, I'll figure out how to actually publish this as a Godot addon!

You'll need a way to actually run the signalling server! The defaults for the Lobby scene are localhost -- this works fine for development but of course doesn't work at all if you want to deploy to itch.io or the like. For that, you'll need a real server running headless godot. TODO: link to some basic tutorials on doing this.

The Lobby also defaults to using one of Google's STUN servers. Beyond demos, you'll almost certainly need to run your own STUN server too! TODO: link to related docs.

## Limitations

 * This only handles 2p. 
 * It gives you only a single data channel. 
 * It doesn't allow for 'high level multiplayer' via the `@rpc` decorators.
 * What you *do* with the channel is up to you, you'll need game specific logic for interpreting the messages.
 * Anything related to disconnect/reconnect/lag-measurement is up to you.

## Reference

Quite a useful article on doing this from 2021, which sadly was a little out of date since it was done with Godot3, and in my opinion a little overcomplicated structurally: https://perons.writeas.com/creating-a-peer-to-peer-snake-game-with-godot-webrtc

The official demo, which is again useful but not even close to "drop in" and intermingles too much between game code and RTC code: https://github.com/godotengine/godot-demo-projects/tree/master/networking/webrtc_signaling. It's also very clear how the 2021 article pulls from this demo in the use of Websockets to setup the initial connection.

A *two hour youtube video* on the topic: https://www.youtube.com/watch?v=ulfGNtiItnc, which again uses similar techniques. This was particularly helpful to me to brush away some of my confusion midway through this effort. It more properly sets up a _mesh_, which is something I tried and failed to get working correctly. Notably this video has some material related to running a signaling server in the cloud, if that's an foreign topic to you.

The official docs were invaluable but it'd be hard to understand all the concepts just from them: https://docs.godotengine.org/en/stable/classes/class_webrtcpeerconnection.html and https://docs.godotengine.org/en/stable/classes/class_webrtcpeerconnection.html

All of this material was useful in helping me grok the concepts. If that's your game, go for it, but to re-iterate the overall goal of this project is that you shouldn't have to re-implement the RTC dance to enjoy low-latency networking.

## TODO

 * multiple data channels
